﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _Speed;
    [SerializeField] private float _JumpForce;
    [SerializeField] private Animator anim;


    private Rigidbody2D _Rb;
    private bool canJump = false;

    void Start()
    {
        _Rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

   
    void Update()
    {
        PlayerMovement();
        PlayerJump();
        
    }

    void PlayerJump()
    {
        if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("isJumping", true);
        }
        else
        {
            anim.SetBool("isJumping", false);
        }
    }
   


    private void PlayerMovement()
    {
        float mov = Input.GetAxis("Horizontal") * _Speed;
        transform.position += new Vector3(mov*Time.deltaTime,0,0);

        if(mov > 0)
        {
            transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        else if(mov < 0)
        {
            transform.rotation = new Quaternion(0, 180, 0, 1);
        }

        if (mov != 0)
        {
            anim.SetBool("isRunning", true);
        } else
        {
            anim.SetBool("isRunning", false);
        }

        if (Input.GetKeyDown(KeyCode.W) && canJump == true)
        {   
            _Rb.AddForce(new Vector2(0, _JumpForce), ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            anim.SetBool("takeDamage", true);
        } 

        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            anim.SetBool("takeDamage", false);
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = false;
        }
    }
}
